package ru.landscape.service.landscape.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.landscape.domain.LandscapeCalculateResult;
import ru.landscape.service.landscape.LandscapeService;
import ru.landscape.service.landscape.calculator.CalculatorService;
import ru.landscape.service.landscape.generator.LandscapeGeneratorService;
import ru.landscape.service.landscape.validator.LandscapeValidatorService;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class LandscapeServiceImpl implements LandscapeService {

    private final CalculatorService calculator;
    private final LandscapeGeneratorService generator;
    private final LandscapeValidatorService validator;


    @Override
    public LandscapeCalculateResult getWaterAmount(int size) {

        var landscape = generator.gen(size);
        return this.getWaterAmount(landscape);

    }

    @Override
    public LandscapeCalculateResult getWaterAmount(int[] landscape) {

        validator.landscapeValid(landscape);

        return LandscapeCalculateResult.builder()
                .landscape(landscape)
                .amount(calculator.calc(landscape))
                .build();

    }



}
