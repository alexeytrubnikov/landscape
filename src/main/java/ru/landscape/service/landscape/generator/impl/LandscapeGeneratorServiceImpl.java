package ru.landscape.service.landscape.generator.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.landscape.service.landscape.generator.LandscapeGeneratorService;

import java.util.Random;

@Service
public class LandscapeGeneratorServiceImpl implements LandscapeGeneratorService {

    @Value("${app.landscape.height.max}")
    private int max;

    @Value("${app.landscape.height.min}")
    private int min;

    @Override
    public int[] gen(int size) {

        return new Random()
                .ints(size, min, max + 1)
                .toArray();

    }

}
