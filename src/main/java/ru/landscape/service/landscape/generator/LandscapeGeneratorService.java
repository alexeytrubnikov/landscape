package ru.landscape.service.landscape.generator;

public interface LandscapeGeneratorService {

    int[] gen(int size);

}
