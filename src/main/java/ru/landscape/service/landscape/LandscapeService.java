package ru.landscape.service.landscape;

import ru.landscape.domain.LandscapeCalculateResult;

public interface LandscapeService {

    LandscapeCalculateResult getWaterAmount(int size);
    LandscapeCalculateResult getWaterAmount(int[] landscape);

}
