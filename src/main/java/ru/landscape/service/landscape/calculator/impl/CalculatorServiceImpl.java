package ru.landscape.service.landscape.calculator.impl;

import org.springframework.stereotype.Service;
import ru.landscape.service.landscape.calculator.CalculatorService;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class CalculatorServiceImpl implements CalculatorService {
    @Override
    public int calc(int[] landscapeParam) {

        AtomicInteger amount = new AtomicInteger();

        Optional.ofNullable(landscapeParam)
                .ifPresent(landscape -> {
                    int rightPoint = landscape.length - 1;
                    int leftPoint = 0;

                    int leftMax = landscape[leftPoint];
                    int rightMax = landscape[rightPoint];

                    while (leftPoint < rightPoint) {

                        if (leftMax >= rightMax) {

                            amount.addAndGet((rightMax - landscape[rightPoint]));
                            rightPoint--;
                            rightMax = Math.max(rightMax, landscape[rightPoint]);

                        } else {

                            amount.addAndGet((leftMax - landscape[leftPoint]));
                            leftPoint++;
                            leftMax = Math.max(leftMax, landscape[leftPoint]);

                        }

                    }

                });

        return amount.get();

    }

}
