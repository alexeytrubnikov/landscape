package ru.landscape.service.landscape.calculator;

import org.springframework.stereotype.Service;
import ru.landscape.domain.LandscapeCalculateResult;

@Service
public interface CalculatorService {

    int calc(int[] landscape);

}
