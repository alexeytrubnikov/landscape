package ru.landscape.service.landscape.validator.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.landscape.service.landscape.validator.LandscapeValidatorService;

import java.util.Arrays;
import java.util.Optional;

@Service
public class LandscapeValidatorServiceImpl implements LandscapeValidatorService {

    @Value("${app.landscape.height.max}")
    private int heightMax;

    @Value("${app.landscape.height.min}")
    private int heightMin;

    @Value("${app.landscape.size.max}")
    private int sizeMax;

    @Override
    public void landscapeValid(int[] landscape) {

        Optional.ofNullable(landscape)
                .orElseThrow(() -> new RuntimeException("landscape is null"));

        if (landscape.length == 0) {
            throw new RuntimeException("landscape is empty");
        }

        if (landscape.length > sizeMax) {
            throw new RuntimeException(String.format("landscape size cannot be more %s", sizeMax));
        }

        Arrays.stream(landscape).forEach(element -> {
            if (element > heightMax) {
                throw new RuntimeException(String.format("landscape element cannot be more %s", heightMax));
            }
            if (element < heightMin) {
                throw new RuntimeException(String.format("landscape element cannot be less %s", heightMax));
            }
        });


    }
}
