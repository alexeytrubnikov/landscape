package ru.landscape.service.landscape.validator;

public interface LandscapeValidatorService {
    void landscapeValid(int[] landscape);
}
