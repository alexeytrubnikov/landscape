package ru.landscape.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@EqualsAndHashCode
public class LandscapeCalculateResult {
    private int[] landscape;
    private int amount;
}
