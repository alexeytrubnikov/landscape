package ru.landscape.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.landscape.domain.LandscapeCalculateResult;
import ru.landscape.service.landscape.LandscapeService;

@RestController
@RequestMapping(value = "/landscape")
@RequiredArgsConstructor
public class LandscapeController {

    private final LandscapeService landscapeService;

    @GetMapping(value = "/{landscapeSize}")
    public LandscapeCalculateResult getAmountResult(@PathVariable("landscapeSize") int size) {

        return landscapeService.getWaterAmount(size);

    }

    @GetMapping
    public LandscapeCalculateResult getAmountResult(@RequestParam("landscape") int[] landscape) {

        return landscapeService.getWaterAmount(landscape);

    }

}
