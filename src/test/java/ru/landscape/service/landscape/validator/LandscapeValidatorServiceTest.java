package ru.landscape.service.landscape.validator;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import ru.landscape.service.landscape.validator.impl.LandscapeValidatorServiceImpl;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@SpringBootTest
@TestPropertySource(properties = {
        "app.landscape.size.max=15",
        "app.landscape.height.max=10",
        "app.landscape.height.min=5",
})
class LandscapeValidatorServiceTest {

    @Autowired
    private LandscapeValidatorService validator;

    @Test
    void testLandscapeValid() {

        //GIVEN
        var landscape = new int[]{8, 7, 8, 6, 5, 9, 9, 7, 6};
        //WHEN
        validator.landscapeValid(landscape);
        //THEN
        assertTrue(true);

    }


    @Test
    void testLandscapeMaxSizeNotValid() {

        //GIVEN
        var landscape = new int[20];
        //WHEN
        var ex = assertThrows(RuntimeException.class, () -> validator.landscapeValid(landscape));
        //THEN
        assertTrue(ex.getMessage().contains("size cannot be more"));

    }

    @Test
    void testLandscapeIsnullNotValid() {

        //GIVEN
        int[] landscape = null;
        //WHEN
        var ex = assertThrows(RuntimeException.class, () -> validator.landscapeValid(landscape));
        //THEN
        assertTrue(ex.getMessage().contains("is null"));

    }

    @Test
    void testLandscapeMinHeightNotValid() {

        //GIVEN
        var landscape = new int[]{10, -1, 8};
        //WHEN
        var ex = assertThrows(RuntimeException.class, () -> validator.landscapeValid(landscape));
        //THEN
        assertTrue(ex.getMessage().contains("element cannot be less"));

    }

    @Test
    void testLandscapeMaxHeightNotValid() {

        //GIVEN
        var landscape = new int[]{10, 150_000, 5};
        //WHEN
        var ex = assertThrows(RuntimeException.class, () -> validator.landscapeValid(landscape));
        //THEN
        assertTrue(ex.getMessage().contains("element cannot be more"));

    }

    @Test
    void testLandscapeIsEmptyNotValid() {

        //GIVEN
        var landscape = new int[]{};
        //WHEN
        var ex = assertThrows(RuntimeException.class, () -> validator.landscapeValid(landscape));
        //THEN
        assertTrue(ex.getMessage().contains("is empty"));

    }

}