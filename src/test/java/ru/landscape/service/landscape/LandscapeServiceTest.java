package ru.landscape.service.landscape;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.landscape.domain.LandscapeCalculateResult;
import ru.landscape.service.landscape.calculator.CalculatorService;
import ru.landscape.service.landscape.generator.LandscapeGeneratorService;
import ru.landscape.service.landscape.impl.LandscapeServiceImpl;
import ru.landscape.service.landscape.validator.LandscapeValidatorService;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class LandscapeServiceTest {

    @Mock
    CalculatorService calculator;
    @Mock
    LandscapeGeneratorService generator;
    @Mock
    LandscapeValidatorService validator;

    @InjectMocks
    LandscapeServiceImpl landscapeService;

    @Test
    void testLandscapeFixSize() {

        //GIVEN
        var landscape = new int[]{5, 2, 3, 4, 5, 4, 0, 3, 1};
        var amount = 9;

        var res = LandscapeCalculateResult.builder()
                .landscape(landscape)
                .amount(amount)
                .build();

        //WHEN
        doReturn(landscape)
                .when(generator)
                .gen(anyInt());

        doReturn(amount)
                .when(calculator)
                .calc(landscape);

        doNothing().when(validator)
                .landscapeValid(any());

        var result = landscapeService.getWaterAmount(9);

        //THEN
        assertEquals(res, result);
        verify(generator, times(1)).gen(anyInt());
        verify(calculator, times(1)).calc(any());
        verify(validator, times(1)).landscapeValid(any());

    }

    @Test
    void testLandscapeGeneral() {

        //GIVEN
        var landscape = new int[]{5, 2, 3, 4, 5, 4, 0, 3, 1};
        var amount = 9;

        var res = LandscapeCalculateResult.builder()
                .landscape(landscape)
                .amount(amount)
                .build();

        //WHEN
        doReturn(amount)
                .when(calculator)
                .calc(landscape);

        doNothing().when(validator)
                .landscapeValid(any());

        var result = landscapeService.getWaterAmount(landscape);

        //THEN
        assertEquals(res, result);
        verify(generator, times(0)).gen(anyInt());
        verify(calculator, times(1)).calc(any());
        verify(validator, times(1)).landscapeValid(any());

    }

}