package ru.landscape.service.landscape.calculator;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.Extensions;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.landscape.domain.LandscapeCalculateResult;
import ru.landscape.service.landscape.calculator.impl.CalculatorServiceImpl;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class CalculatorServiceTest {

    private final CalculatorServiceImpl calculator = new CalculatorServiceImpl();

    @Test
    void successCalc() {

        //GIVEN
        var amount = 9;
        var landscape = new int[]{5, 2, 3, 4, 5, 4, 0, 3, 1};

        //WHEN
        var calcResult = calculator.calc(landscape);

        //THEN
        assertEquals(amount, calcResult);

    }

}